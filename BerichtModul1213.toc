\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Aufgabenstellung}{1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Systemanalyse}{1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Gesamtblockschaltbild}{1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Wirkungskette}{2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Recherche der verwendeten Komponenten}{2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Konzeptentwicklung}{2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Viertelbrücke und Differenzverstärker}{2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Viertelbrücke und Instrumentenverstärker}{2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Spannungsteiler mit nichtinvertierendem Operationsverstärker}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Schaltungsentwicklung}{3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Simulation mit LT-Spice}{4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Auswertung mit Matlab}{4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}Interpretation}{4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11}Ausblick}{4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {11.1}Probleme}{4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {11.2}Kostenaufstellung}{4}%
