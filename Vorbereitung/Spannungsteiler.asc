Version 4
SHEET 1 888 680
WIRE 256 32 80 32
WIRE 384 32 256 32
WIRE 496 32 384 32
WIRE 256 64 256 32
WIRE 576 96 448 96
WIRE 720 96 656 96
WIRE 80 128 80 32
WIRE 496 144 496 32
WIRE 384 160 384 112
WIRE 416 160 384 160
WIRE 448 160 448 96
WIRE 448 160 416 160
WIRE 464 160 448 160
WIRE 656 176 528 176
WIRE 720 176 720 96
WIRE 720 176 656 176
WIRE 256 192 256 144
WIRE 464 192 256 192
WIRE 384 224 384 160
WIRE 80 304 80 208
WIRE 256 304 256 272
WIRE 256 304 80 304
WIRE 384 304 256 304
WIRE 496 304 496 208
WIRE 496 304 384 304
WIRE 80 320 80 304
FLAG 80 320 0
FLAG 656 176 Uout
FLAG 80 32 V+
FLAG 416 160 Uoff
SYMBOL voltage 80 112 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 5
SYMBOL res 240 176 R0
WINDOW 3 109 76 Right 2
SYMATTR Value {PTC}
SYMATTR InstName R2
SYMBOL res 400 320 R180
WINDOW 0 -44 81 Left 2
WINDOW 3 -53 41 Left 2
SYMATTR InstName R4
SYMATTR Value 8.5k
SYMBOL res 672 80 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 12.2k
SYMBOL res 240 48 R0
SYMATTR InstName R1
SYMATTR Value {R1}
SYMBOL res 368 16 R0
SYMATTR InstName R5
SYMATTR Value {R1}
SYMBOL Opamps\\AD8538 496 112 R0
SYMATTR InstName U1
TEXT 48 344 Left 2 !;tran 1
TEXT 48 384 Left 2 !.param R1=24.19230769k
TEXT 48 432 Left 2 !.param ptc=ptc0+T*ptc1+T*T*ptc2
TEXT 48 480 Left 2 !.param ptc0=3.1302k
TEXT 48 520 Left 2 !.param ptc1=-0.0169k
TEXT 48 560 Left 2 !.param ptc2=0.00013419k
TEXT 48 640 Left 2 !.param T0=273.15
TEXT 48 600 Left 2 !.param T=x+T0
TEXT 632 536 Left 2 !.step param x 0 100 1
TEXT 48 664 Left 2 !.tran 1
